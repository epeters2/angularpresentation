import { Injectable, OnInit } from '@angular/core';
import { House } from '../houses/house.model';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HouseService implements OnInit {
  url: string;
  errorMessage: string;
  loadedHouses: House[];

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };


  constructor(private httpClient: HttpClient) {
    this.url = "http://localhost:3000/houses";
    this.loadedHouses = [];
  }

  getHouses(): Observable<House[]> {
    return this.httpClient.get<House[]>(this.url).pipe(tap(data => console.log('All ' + JSON.stringify(data))), catchError(this.handleError));
    // return [
    //   new House(1, "123 Maple Street", "bootyful house", 234000, new Date(2019, 8, 18), 4, 3.5, "4237777777", "./assets/images/house1.jpeg",4.5),
    //   new House(2, "234 Maple Street", "bootyful house", 567000, new Date(2019, 8, 18), 4, 3, "1234567891", "./assets/images/house2.jpeg",5),
    //   new House(3, "256 Maple Street", "bootyful house", 277900, new Date(2019, 8, 18), 4, 3, "4237040000", "./assets/images/house3.jpeg",3.9),
    //   new House(4, "999 Maple Street", "bootyful house", 277900, new Date(2019, 8, 18), 4, 3, "4237040001", "./assets/images/house4.jpeg",2.8),
    //   new House(5, "1000 Bomar St", "bootyful house", 277900, new Date(2019, 8, 18), 4, 3, "4237040002", "./assets/images/house5.jpeg",3),
    //   new House(6, "47 Main St", "bootyful house", 277900, new Date(2019, 8, 18), 4, 3, "4237040003", "./assets/images/house6.jpeg",1),
    //   new House(7, "99 North Wellwood Ave", "bootyful house", 277900, new Date(2019, 8, 18), 4, 3, "4237040004", "./assets/images/house7.jpeg",4.2)
    // ];
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

  getHouseBy(id: number): Observable<House> {
    //let houses:House[] = this.getHouses();
    //var filteredResults = houses.filter(house => house.id == id);
    //return filteredResults[0];

    //this.httpClient.get<House>(this.url).pipe(tap(data => console.log('All ' + JSON.stringify(data))), catchError(this.handleError));
    //^^ but add id to end of house.

    var newUrl = this.url + "/" + id;
    return this.httpClient.get<House>(newUrl).pipe(tap(data => console.log('Got ' + JSON.stringify(data))), catchError(this.handleError));


    //var filteredResults = this.loadedHouses.filter(house => house.id == id);
    //return filteredResults[0];

  }

  ngOnInit(): void {
    this.getHouses().subscribe(houses => {
      this.loadedHouses = houses;
    },
      error => this.errorMessage
        = error
    )
  }

  createHouse(house: House): Observable<House> {
    return this.httpClient.post<House>(this.url, house, this.httpOptions).pipe(tap((house) => { console.log("POST call successful value returned in body", house); }), catchError(this.handleError));
  }

  deleteHouse(id: number): Observable<void> {
    var deleteUrl = this.url + "/" + id;
    return this.httpClient.delete<void>(deleteUrl, this.httpOptions).pipe(tap(() => { console.log("deleted house " + id), error => this.errorMessage = error}));
  }

  editHouse(id: number, editedHouse:House): Observable<House> {
    var editUrl = this.url + "/" + id;
    return this.httpClient.put<House>(editUrl, editedHouse, this.httpOptions).pipe(tap((house) => { console.log("PUT call sucessful, value returned in body", house); }), catchError(this.handleError));
  }
}
