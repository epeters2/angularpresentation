import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router'

import { AppComponent } from './app.component';
import { HouseListComponent } from './houses/house-list.component';
import { House } from './houses/house.model';
import { HomeComponentComponent } from './houses/home-component.component';
import { ConvertPhonePipe } from './shared/convert-phone.pipe';
import { FilteringPipe } from './shared/filtering.pipe';
import { MoveDayToEndPipe } from './shared/move-day.pipe';
import { StarComponent } from './shared/star.component';
import { BathroomComponent } from './shared/bathroom.component';
import { HomeComponent } from './home/home.component';
import { MenuComponent } from './menu/menu.component';
import { HouseDetailComponent } from './houses/house-detail.component';
import {HttpClientModule} from '@angular/common/http';
import { HouseCreateComponent } from './home/house-create.component';
import { HouseEditComponent } from './home/house-edit.component'

@NgModule({
  declarations: [
    AppComponent,
    HouseListComponent,
    HomeComponentComponent,
    ConvertPhonePipe,
    FilteringPipe,
    MoveDayToEndPipe,
    StarComponent,
    BathroomComponent,
    HomeComponent,
    MenuComponent,
    HouseDetailComponent,
    HouseCreateComponent,
    HouseEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path:'2019', children: [
        {path: '', component:HomeComponent, children: [
          {path: 'preview', component:HouseListComponent},
        ]},
        {path: 'houses', children: [
          {path: '', component:HouseListComponent},
          {path: 'create', component:HouseCreateComponent},
          {path: ':id', children: [
            {path: '', component:HouseDetailComponent },
            {path: 'edit', component: HouseEditComponent}
          ]},
        ]},
      ]},
      {path:'', redirectTo:'2019', pathMatch:'prefix'},
      {path:'**', redirectTo:'2019', pathMatch:'prefix'}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
