import { Component, OnInit } from '@angular/core';
import { House } from '../houses/house.model';
import { NgForm } from '@angular/forms';
import { HouseService } from '../services/house.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-house-create',
  templateUrl: './house-create.component.html',
  styleUrls: ['./house-create.component.css']
})
export class HouseCreateComponent implements OnInit {

  house:House;
  diagnostic:String;
  returnedHouse:House;
  errorMessage:String;

  constructor(private houseService:HouseService, private route:Router) { 
    this.house = new House(null, null, "Banch", 9001, new Date(1997, 1, 6), 3, 1, "4237040000", "./assets/images/house4.jpeg", 3.7, 0);
  }

  ngOnInit() {

  }

  onSubmit(form: NgForm):void {
    console.log(form);
    //this.house = new House()
    this.diagnostic = JSON.stringify(this.house);
    this.houseService.createHouse(this.house).subscribe((returnValue) => this.route.navigate(["/houses"]), error => this.errorMessage);
    //this.route.navigate(["/houses"]);
  }

}
