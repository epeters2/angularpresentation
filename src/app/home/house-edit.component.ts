import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HouseService } from '../services/house.service';
import { House } from '../houses/house.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-house-edit',
  templateUrl: './house-edit.component.html',
  styleUrls: ['./house-edit.component.css']
})
export class HouseEditComponent implements OnInit {

  house:House;
  errorMessage:string;

  constructor(private activatedRoute: ActivatedRoute, private houseService:HouseService, private router:Router) { 
    
  }

  ngOnInit() {
    let id = +this.activatedRoute.snapshot.paramMap.get("id");
    this.houseService.getHouseBy(id).subscribe(house => {
      this.house = house;
    },
    error => this.errorMessage = error
    );
  }

  onSubmit(form: NgForm):void {
    console.log(form);
    var id = this.house.id;
    this.houseService.editHouse(id ,this.house).subscribe((returnValue) => this.router.navigate(["/houses"]), error => this.errorMessage);
  }

}
