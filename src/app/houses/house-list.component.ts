import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { House } from './house.model';
import { HouseService } from '../services/house.service';
import { Browser } from 'protractor';
import { Router } from '@angular/router';

@Component({
    selector: 'lm-houselist',
    templateUrl: './house-list.component.html',
    styleUrls: ['./house-list.component.css']
})
export class HouseListComponent implements OnInit, OnChanges{
    @Input() parentPageTitle:string;
    isDisabled:boolean = true;
    isIdVisible:boolean = true;
    private _searchTerm: string;
    childInfo:string;
    url:string;
    errorMessage:string;

    

    houses:House[]; //=
    //[
        // new House(1, "123 Maple Street", "bootyful house", 234000, new Date(2019, 8, 18), 4, 3, "4237777777", "./assets/images/house1.jpeg",4.5),
        // new House(2, "234 Maple Street", "bootyful house", 567000, new Date(2019, 8, 18), 4, 3, "1234567891", "./assets/images/house2.jpeg",5),
        // new House(3, "256 Maple Street", "bootyful house", 277900, new Date(2019, 8, 18), 4, 3, "4237040000", "./assets/images/house3.jpeg",3.9),
        // new House(4, "999 Maple Street", "bootyful house", 277900, new Date(2019, 8, 18), 4, 3, "4237040001", "./assets/images/house4.jpeg",2.8),
        // new House(5, "1000 Bomar St", "bootyful house", 277900, new Date(2019, 8, 18), 4, 3, "4237040002", "./assets/images/house5.jpeg",3),
        // new House(6, "47 Main St", "bootyful house", 277900, new Date(2019, 8, 18), 4, 3, "4237040003", "./assets/images/house6.jpeg",1),
        // new House(7, "99 North Wellwood Ave", "bootyful house", 277900, new Date(2019, 8, 18), 4, 3, "4237040004", "./assets/images/house7.jpeg",4.2)
    //];

    constructor(private houseService:HouseService){
        //this.houses = houseService.getHouses();
        this.searchResults = this.houses;
        console.log("in constructor, parentPageTitle = " + this.parentPageTitle);
    }

    get searchTerm():string{
        return this._searchTerm;
    }

    set searchTerm(term:string){
        this._searchTerm = term;
        this.onSearchClicked();
    }

    searchResults:House[] = this.houses;

    onShowIdClicked():void{
        this.isIdVisible = !this.isIdVisible;
    }

    onSearchClicked():void{
        this.searchResults = this.searchTerm && this.searchTerm.length>0 ?
            this.houses.filter(house => house.address.toLowerCase().includes(this.searchTerm))
            : this.houses;
    }

    onAdd(){
        this.houses.push(new House(7, "99 North Wellwood Ave", "bootyful house", 277900, new Date(2019, 8, 18), 4, 3, "4237040004", "./assets/images/house7.jpeg",4, 0))
    }

    ngOnInit(): void {
        console.log("in ngOnInit, parentPageTitle = " + this.parentPageTitle);
        this.getHousesFromJson();
    }

    ngOnChanges(): void {
        console.log("in ngOnChanges, parentPageTitle = " + this.parentPageTitle);
        location.reload();
    }

    handleChildEvent(payload:string) :void {
        this.childInfo = payload;
    }

    onDelete(id:number): void {
        this.houseService.deleteHouse(id).subscribe(() => this.getHousesFromJson(), error => this.errorMessage);
        this.searchResults = this.houses;
    }

    getHousesFromJson() {
        this.houseService.getHouses().subscribe( houses => {
            this.houses = houses;
            this.searchResults = this.houses;
            },
            error => this.errorMessage
            = error
        );
    }

}