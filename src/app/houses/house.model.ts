export class House{
   constructor(public id: number, public address: string, public description: string, public listingPrice: number, public availableDate: Date, public numberOfBedrooms: number, public numberOfBathrooms: number, public contactPhone: string, public imageUrl: string, public rating:number, public likes:number){
       
   }
}