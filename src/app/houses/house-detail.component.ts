import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HouseService } from '../services/house.service';
import { House } from './house.model';

@Component({
  selector: 'app-house-detail',
  templateUrl: './house-detail.component.html',
  styleUrls: ['./house-detail.component.css']
})
export class HouseDetailComponent implements OnInit {

  currentHouse: House;
  //numberOfLikes: number = 0;
  errorMessage:String;

  constructor(private route: ActivatedRoute, private houseService:HouseService, private router:Router) {

   }

  ngOnInit() {
    let id = +this.route.snapshot.paramMap.get("id");
    this.houseService.getHouseBy(id).subscribe(house => {
      this.currentHouse = house;
    },
    error => this.errorMessage = error
    );
  }

  onBack():void {
    var id = this.currentHouse.id;
    this.houseService.editHouse(id ,this.currentHouse).subscribe((returnValue) => this.router.navigate(["/houses"]), error => this.errorMessage);
  }

  onHeartClicked():void {
    this.currentHouse.likes++;
  }

}