import { PipeTransform, Pipe } from '@angular/core';
import { formatDate } from '@angular/common';

@Pipe ({
    name: 'moveday'
})

export class MoveDayToEndPipe implements PipeTransform{
    transform(value: Date) {
        return formatDate(value, 'MMM d, yyyy, EEEE', 'en-US')
    }

}