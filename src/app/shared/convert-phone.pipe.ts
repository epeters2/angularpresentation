import {Pipe, PipeTransform} from '@angular/core';
import { stringify } from '@angular/compiler/src/util';
@Pipe({
    name: 'convertPhone'
})

export class ConvertPhonePipe implements PipeTransform{
    transform(value: string, param: string): string {
        var returnString = value as string;
        if (returnString.length != 10){
            return "NaN";
        }
        var areaCode = returnString.slice(0, 3);
        var localCode = returnString.slice(3, 6);
        var signalCode = returnString.slice(6, 10);
        return areaCode + "-" + localCode + "-" + signalCode;

    }

}