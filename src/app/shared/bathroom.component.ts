import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-bathroom',
  templateUrl: './bathroom.component.html',
  styleUrls: ['./bathroom.component.css']
})
export class BathroomComponent implements OnInit, OnChanges {
  @Input() numberOfBathrooms: number;
  componentMaxWidth:number;

  bathrooms:number[] = [];


  constructor() {

   }

  ngOnInit() {

  }

  ngOnChanges() {
    for (var i = 0; i < this.numberOfBathrooms; i++){
      var newNumber = 0;
      this.bathrooms.push(newNumber);
    }
    this.componentMaxWidth = 18 * this.numberOfBathrooms;
  }

}
