import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-star',
  templateUrl: './star.component.html',
  styleUrls: ['./star.component.css']
})
export class StarComponent implements OnInit, OnChanges {

  @Input() rating: number;
  @Output() childEvent: EventEmitter<string>= new EventEmitter<string>();
  starWidth:number;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(): void {
    this.starWidth = (90 * this.rating) / 5;
  }

  onStarClick(): void {
    this.childEvent.emit(`The rating ${this.rating} was clicked`)
  }


}
